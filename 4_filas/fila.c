#include "ls.h"
#include "fila.h"
#include "stdlib.h"

#define MAX 5
        
struct fila * createQ(){
	        
	struct fila *f;
	
	f = malloc(sizeof(struct fila));
	
	f->l = create(MAX);
	f->inicio = 0;
	f->fim = 0;
	f->qtd = 0;
	
	return f;						
}

int makenull(struct fila * f){
	int i;
	if(f == NULL)
		return 0;
		
	else{	
		while (!vazia(f))
		{
			removel(f->l, f->inicio);
			f->qtd--;
			f->inicio--;
			i++;					
		}
	}
	return 1;		
}

int dequeue(struct fila * f){
	
	if(f == NULL)
		return 0;
		
	else
	{
		if(vazia(f))
			return 0;
			
		else
		{
			 removel(f->l, f->inicio);
			 f->inicio=(f->inicio +1 % MAX);
			 f->qtd--;
			 return f->inicio ;
		 		
		}				
	}		 
}

int enqueue(struct fila * f, int val){
	
	if(f == NULL)
		return 0;
		
	else
	{			
		if(f->qtd < MAX){
			insert(f->l, f->fim, val);		
		
			f->fim = (f->fim+1 % MAX);
			f->qtd++;
		
			return 1;
			
		}		
		return 0;							
	}			
}

int vazia(struct fila * f){
	 	 
	 if(f->qtd == 0)
		return 1;
		
	else
		return 0;
							 
}

void destroyQ(struct fila * f){
	
	free(f);
	
}
