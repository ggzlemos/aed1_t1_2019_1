#include "le.h"
#include "pilha.h"
#include "stdlib.h"

struct pilha *create(){
		
	struct pilha *p;
	struct llist *lista;
	
	lista = create_l();
	
	p = malloc(sizeof(struct pilha));
	
	p->l = lista;
	
	return p;	
}

int makenull(struct pilha * p){
	
	while(! vazia(p)){
		delete_l(p->l, p->l->cabeca);
	}
	
	return 1;
	
}

int push(struct pilha * p, int val){
	
	struct elem *t;
	
	t = create_node(val);
					
	if(p == NULL)
		return 0;
		
	else{	
		insert_l(p->l, p->l->cabeca, t);
		return 1;			
	}	
}

int top(struct pilha * p){

		if (p == NULL)
		{
			return 0;
		}
		
		else
			return length_l(p->l);;
}	

    
int pop(struct pilha * p){
	
	struct elem *aux;
	
	if(p == NULL)
		return 0;
		
	else
	{
		aux = p->l->cabeca;
		//delete_l(p->l, p->l->cabeca);
		p->l->cabeca = p->l->cabeca->next;
		free(aux);
		p->l->tam--;
		return 1;
		
	}	 		
}

int vazia(struct pilha *p){
	
	if(p == NULL)
		return 0;
	if(length_l(p->l) == 0)
		return 1;
		
	else	
		return 0;	
	
}

void destroy(struct pilha * p){
	free(p);
}


