#include "ls.h"
#include "stdlib.h"

/* Aqui devem ser implementadas as funções definidas em ls.h */
struct list * create(int max){		
	struct list *l;   
	elem *e;
	
	if (max <= 0)
	{
		return NULL;
	}
	else
	{		
		l = (malloc(sizeof(struct list)));
		
		e = (malloc(sizeof(elem)*max));
		
		l->arm = e;
		l->capacidade=max; 
		l->ultimo = 0;
	
	}	
	return l;
}

int insert(struct list *desc, int pos, elem item){
		
	int i;
		
		if (desc->capacidade <= desc->ultimo)
		{
			return 0;
	
		}
		else{
			if (pos > desc->ultimo +1 || pos < 1 )
			{
				return 0;	
			}

			else
			{
				for (i = desc->ultimo; i < pos ; i++)
				{
					desc->arm[i+1] = desc->arm[i];
				}
			
				desc->arm[pos] = item;
				desc->ultimo = desc->ultimo +1;

				return 1;	
			}
			
	}		
}

int removel(struct list *desc, int pos){
	
	int i;
	
	if (pos > desc->ultimo )
	{
		return 0;
	}
	
	else
	{
		desc->ultimo = desc->ultimo -1;
		
		for (i = pos; i < desc->ultimo; i++)
		{
			desc->arm[i] = desc->arm[i+1];
		}	
		
		return 1;
	}
	
}

elem get(struct list *desc, int pos){
		
	return desc->arm[pos];	

}


int set(struct list *desc, int pos, elem item){
	
	if (pos > desc->ultimo )
	{
		return 0;
	}
		
	else{
		desc->arm[pos] = item;
		return 1;
	}
	 	
}

int locate(struct list *desc, int pos, elem item){
	int i;
	
	for (i = pos; i <= desc->ultimo; i++)
	{
		if(desc->arm[i] == item)
			return i;
	}	
	
	return 0;	
}

int length(struct list *desc){
	
	return desc->ultimo;
}


int max(struct list *desc){
	return desc->capacidade;
}


int full(struct list *desc){
	if(length(desc) == max(desc))
		return 1;
		
	else
		return 0;	
	
}

void destroy(struct list *desc){
		desc->ultimo=0;
}


