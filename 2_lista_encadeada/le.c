#include "le.h"
#include "stdlib.h"
/* Aqui devem ser implementadas as fun��es definidas em le.h */

struct llist * create_l(){
	
	struct llist *lista;
	lista = malloc(sizeof(struct llist));
	
	lista->tam = 0;
	lista->cabeca =NULL;
				
	return lista;        

}

elem * create_node(int val){
	
		struct elem *l;
		l = malloc(sizeof(struct elem));
		
		l->val = val;
		l->next = NULL;
		
		
		return l;				
}

int insert_l(struct llist *desc, elem * prev, elem * item){
				
	if (desc == NULL)
	{
		return 0;
	}
	
	else
	{
		if(item == NULL)
			return 0;
			
		else{
			
			if(desc->tam ==0){
				item->next =NULL;
				desc->cabeca = item;	
				desc->tam++;
				return 1;	
			}
			
			else
			{
				if(prev == NULL){
					
					item->next = desc->cabeca;
					desc->cabeca = item;
					desc->tam++;
					
					return 1;								
				}
				else{
					item->next = prev->next;
					prev->next = item;
					desc->tam++;
					return 1;
				}	
			}
				
		}		
	}
	return 0;
			
}

int delete_l(struct llist *desc, elem * prev){
		
	struct elem *p;
	struct elem *q;
	
	if(prev == NULL){
			q = desc->cabeca;
			p = q;
			free(q);
			return 1;
	}
	else{
		p = prev;
		q = p->next;
			
		if (desc == NULL)
		{
			return 0;
		}
				
	
		p = q->next;
		free(q);
		desc->tam--;
			
			
		return 1;
				
	}				
}				


elem * get_l(struct llist *desc, int pos){
	
	struct elem *l;	
	int i = 1;
	
	if (desc == NULL)
	{
		return NULL;
	}
	
	else
	{
		if (pos < 1 || pos > desc->tam)
		{
			return NULL;	
		}
	}
	
	l = desc->cabeca;
	
	while (i < pos && l != NULL)
	{
		l=l->next;
		i++;
	
	}
	
	return l;				
}

int set_l(struct llist *desc, int pos, int val){
	
	struct elem *l;	
	int i = 1;
	
	if (desc == NULL)
	{
		return 0;
	}
	
	else
	{
		if (pos < 1 || pos > desc->tam )
		{
			return 0;	
		}
	}
	
	l = desc->cabeca;
	
	while (i < pos )
	{
		l=l->next;
		i++;
			
	}
		
	l->val = val;	
	return 1;	
	
}

elem * locate_l(struct llist *desc, elem * prev, int val){
	
	struct elem *l;
				
	if(desc == NULL)
		return NULL;
		
	else
	{
		if(prev == NULL){
			
			l = desc->cabeca;
			
			while(l != NULL && l->val != val){
				
				l=l->next;					
			}
			
			return l;				
		}					
		else
		{
			l = prev;
			
			while (l != NULL )
			{
				if(l-> val == val)
					return l;
					
															
				l=l->next;								
			}
			
			return NULL;
			
		}	
		return NULL;			
	}
}
int length_l(struct llist *desc){
	
	return desc->tam;	
}

void destroy_l(struct llist *desc){
	
	free(desc);	
}

